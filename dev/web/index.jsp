<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Geniet! etos</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="./css/normalize.css" type="text/css">
    <link rel="stylesheet" href="./css/stylesheet.css" type="text/css">

    <script src="./js/jquery-1.8.3.min.js"></script>
    <script src="./js/jquery.cycle2.min.js"></script>
    <script src="./js/global.js"></script>

    <script type="text/javascript">

        $(document).ready(function(){


        });

    </script>
</head>
<body class="scene1">

<div id="container">

    <header>
        <img src="./img/geniet-etos.png" alt="">
        <h1>persoonlijk geschenk</h1>
        <h2>Maak van uw cadeau een<br> bijzonder en persoonlijk geschenk</h2>
    </header>


    <section id="slider" class="cycle-slideshow" data-cycle-slides="article" data-cycle-fx="fade" data-cycle-sync="false" data-cycle-pager="#slider-control" data-cycle-pager-template="<li><a href=#> {{slideNum}} </a></li>"
            >
        <article>
            <img src="./img/slide1.png" alt="" />
        </article>
        <article>
            <img src="./img/slide2.png" alt="" />
        </article>
    </section>

    <ul id="slider-control"></ul>

    <a href="start.jsp" title="" class="button green" id="proceed">Start hier</a>

</div>

</body>
</html>