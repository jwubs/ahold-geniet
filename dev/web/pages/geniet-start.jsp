<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Geniet! etos</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="../css/normalize.css" type="text/css">
    <link rel="stylesheet" href="../css/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="../css/jquery-ui.css" type="text/css">
    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/jquery.cycle2.min.js"></script>
    <script src="../js/jquery-ui-min.js"></script>
    <script src="../js/idle-timer.js"></script>
    <script src="../js/global.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){

            init(60000);

        });

        function timeExpired()
        {
            location.href="../index.jsp";
        }

        var productScanned = false;
        function productScan()
        {
            if(!productScanned)
            {
                productScanned = true;
                setTimeout(function(){
                    productScanned = false;
                    document.forms[0].submit();
                },300);
            }
        }
    </script>
</head>
<body class="scene3 screen1" onload="document.getElementById('productCode').focus()">

<div id="container">

    <header>
        <img src="../img/geniet-etos.png" alt="">
        <h1>uw parfum</h1>
        <h2>Uw parfum</h2>
    </header>

    <section>

        <div class="steps">
            <img src="../img/bar-step1.png" alt="" class="step">
            <ul class="step-detail step1">
                <li class="hidden">Uw product</li>
                <li>Persoonlijke boodschap</li>
                <li>Het geschenk</li>
            </ul>
        </div>

        <img src="../img/badge-gift.png" alt="" class="badge">

        <article>
            <figure><img src="../img/barcode.png" alt=""></figure>
            <p>Scan nu de barcode om de produktinfo te bekijken</p>
        </article>

    </section>

    <ul id="nav">
        <li><a href="../start.jsp" title="" class="button">Terug</a>
        <li><a href="../index.jsp" title="" class="button">Opnieuw starten</a>
    </ul>
    <form action="GetProduct" method="post">
        <div style="position: absolute; left: -610px; top: -80px; height: 1px; width: 1px; padding: 1em;">
            <input type="text" id="productCode" name="productCode" size="20" onkeypress="productScan()">
        </div>
        <!--
        <input type="button" value="Scan" onclick="document.getElementById('productCode').value='5410013108009';productScan();">
        -->
    </form>
</div>
<div id="dialog">
    <div id="close-dialog"><span>SLUIT VENSTER</span></div>
    <h3>Wilt u doorgaan?</h3>
    <div id="counter"></div>
    <p>Klik dan ‘Ga verder’. Zo niet dan brengen wij u terug naar het  startscherm binnen 10 seconden.</p>
    <a class="button green" onclick="$('#dialog').dialog('close');">GA VERDER</a>
</div>
</body>
</html>