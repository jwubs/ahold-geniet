<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Geniet! etos</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="../css/normalize.css" type="text/css">
    <link rel="stylesheet" href="../css/stylesheet.css" type="text/css">

    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/jquery.cycle2.min.js"></script>
    <script src="../js/global.js"></script>

    <script type="text/javascript">
        var state = 0;
        var percentage = 0;
        var timerTick = 0;
        var vidCountDown = 3;
        var isSaving = false;

        $(document).ready(function(){

            init('homepage');

        });

        function updateTimer()
        {
            if( timerTick <= 12000 )
            {
                timerTick = timerTick + 1;
                percentage = ( 100 * ( timerTick / 12000 ) );
                document.getElementById("progressline").style.width = percentage + "%";
            } else {
                clearInterval(rectimer);
                recordFinished();
            }
        }

        function recordFinished()
        {
            var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
            flash = isInternetExplorer ? genietCam : genietCam2;
            flash.stopRecordVideo('Geniet');

            document.getElementById("recStore").style.display = "block";
            document.getElementById("recViewback").style.display = "block";
            document.getElementById("reRecbutton").style.display = "block";
            document.getElementById("recbutton").style.display = "none";
            document.getElementById("recIndicator").style.display = "none";
            document.getElementById("recProgressbar").style.display = "none";
            document.getElementById("recTimer").style.display = "none";
            document.getElementById("replayIndicator").style.display = "none";
            state = 0;
            percentage = 0;
            timerTick = 0;
        }

        function recordButtonClick()
        {
            var newState = 0;

            if(state==0)
            {
                vidCountDown = 3;
                newState = 1;
                document.getElementById("videoCountdown").style.display = "block";
                document.getElementById("videoCountdown").className = "vidcountdown"+vidCountDown;
                vidtimer = setInterval ( "countDownToRecord()", 1000 );
                document.getElementById("replayIndicator").style.display = "none";
            }

            if(state==1)
            {

                document.getElementById("videoCountdown").style.display = "none";
                document.getElementById("recbutton").className='record red';
                document.getElementById("videoText").style.display = "block";
                document.getElementById("videoBadge").style.display = "block";
                document.getElementById("recIndicator").style.display = "none";
                document.getElementById("recProgressbar").style.display = "none";
                document.getElementById("recTimer").style.display = "none";
                document.getElementById("replayIndicator").style.display = "none";
                newState = 0;
                percentage = 0;
                timerTick = 0;
                recordFinished();
            }

            state = newState;
        }

        function saveVideo()
        {
            isSaving = true;

            var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
            flash = isInternetExplorer ? genietCam : genietCam2;
            flash.stopVideo('Geniet');

            document.getElementById("recStore").style.display = "none";
            document.getElementById("recViewback").style.display = "none";
            document.getElementById("reRecbutton").style.display = "none";
            document.getElementById("recbutton").style.display = "none";
            document.getElementById("recVideoReady").style.display = "block";
            document.getElementById("replayIndicator").style.display = "none";
            setTimeout(
                    function(){
                        document.forms["productVideoForm"].submit();
                    },3000);
        }

        function countDownToRecord()
        {
            if( vidCountDown <= 0 ){
                clearInterval(vidtimer);
                document.getElementById("videoCountdown").style.display = "none";
                initRecord();
            } else {
                vidCountDown = vidCountDown - 1;
                document.getElementById("videoCountdown").className = "vidcountdown"+vidCountDown;
            }
        }

        function initRecord()
        {
            percentage = 0;
            timerTick = 0;
            document.getElementById("recbutton").className='stop';
            document.getElementById("recStore").style.display = "none";
            document.getElementById("recViewback").style.display = "none";
            document.getElementById("reRecbutton").style.display = "none";
            document.getElementById("videoText").style.display = "none";
            document.getElementById("videoBadge").style.display = "none";
            document.getElementById("recbutton").style.display = "block";
            document.getElementById("recIndicator").style.display = "block";
            document.getElementById("replayIndicator").style.display = "none";
            document.getElementById("recProgressbar").style.display = "block";
            document.getElementById("recTimer").style.display = "block";
            document.getElementById("recStore").style.display = "none";
            document.getElementById("recViewback").style.display = "none";

            document.getElementById("progressline").style.width = percentage + "%";
            startRecord();
            rectimer = setInterval ( "updateTimer()", 10 );
        }

        function startRecord()
        {
            var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
            flash = isInternetExplorer ? genietCam : genietCam2;
            flash.startRecordVideo('Geniet');
        }

        function playBackVideo()
        {
            document.getElementById("recStore").style.display = "block";
            document.getElementById("recViewback").style.display = "none";
            document.getElementById("reRecbutton").style.display = "none";
            document.getElementById("recbutton").style.display = "none";
            document.getElementById("recIndicator").style.display = "none";
            document.getElementById("recProgressbar").style.display = "none";
            document.getElementById("recTimer").style.display = "none";
            document.getElementById("replayIndicator").style.display = "block";

            var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
            flash = isInternetExplorer ? genietCam : genietCam2;
            flash.playBackVideo('Geniet');
        }

        function setVideoName(videoName){
            document.getElementById("recordedStream").value = videoName;
        }

        function playBackStopped() {
            if(!isSaving)
            {
                document.getElementById("recStore").style.display = "block";
                document.getElementById("recViewback").style.display = "block";
                document.getElementById("reRecbutton").style.display = "block";
                document.getElementById("recbutton").style.display = "none";
                document.getElementById("recIndicator").style.display = "none";
                document.getElementById("recProgressbar").style.display = "none";
                document.getElementById("recTimer").style.display = "none";
                document.getElementById("replayIndicator").style.display = "none";
            }
        }
    </script>
</head>
<body class="scene4 screen1">

<div id="container">

    <header>
        <img src="../img/geniet-etos.png" alt="">
        <h1>uw boodschap</h1>
        <h2>Neem je persoonlijke boodschap op</h2>
    </header>

    <section>
        <div class="steps">
            <img src="../img/bar-step2.png" alt="" class="step">
            <ul class="step-detail step1">
                <li>Uw product</li>
                <li class="hidden">Persoonlijke boodschap</li>
                <li class="inactive">Het geschenk</li>
            </ul>
        </div>

        <img src="../img/badge-video.png" id="videoBadge" alt="" class="badge">

        <article>
            <div class="video">
                <object id="genietCam" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="768" height="432" align="middle">
                    <param name="movie" value="../video/genietVideo.swf" />
                    <param name=FlashVars value="rtmpUrl=rtmp://localhost/vod/geniet">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="play" value="true" />
                    <param name="loop" value="true" />
                    <param name="wmode" value="window" />
                    <param name="scale" value="showall" />
                    <param name="menu" value="true" />
                    <param name="devicefont" value="false" />
                    <param name="salign" value="" />
                    <param name="allowScriptAccess" value="always" />
                    <!--[if !IE]>-->
                    <object id="genietCam2" type="application/x-shockwave-flash" data="../video/genietVideo.swf" width="768" height="432">
                        <param name="movie" value="../video/genietVideo.swf" />
                        <param name=FlashVars value="rtmpUrl=rtmp://localhost/vod/geniet">
                        <param name="quality" value="high" />
                        <param name="bgcolor" value="#ffffff" />
                        <param name="play" value="true" />
                        <param name="loop" value="true" />
                        <param name="wmode" value="window" />
                        <param name="scale" value="showall" />
                        <param name="menu" value="true" />
                        <param name="devicefont" value="false" />
                        <param name="salign" value="" />
                        <param name="allowScriptAccess" value="always" />
                        <!--<![endif]-->
                        <a href="http://www.adobe.com/go/getflash">
                            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                        </a>
                        <!--[if !IE]>-->
                    </object>
                    <!--<![endif]-->
                </object>
            </div>
            <div id="recVideoReady" class="stored" style="display: none;"><!-- <img src="../img/tick.png" alt=""> -->Uw Boodschap wordt opgeslagen...</div>
            <div id="recIndicator" class="status" style="display: none;"><img src="../img/recording.png" alt=""> recording</div>
            <div id="replayIndicator" class="status" style="display: none;"><img src="../img/replay.png" alt=""></div>
            <div id="recbutton" class="record red" onclick="recordButtonClick()">record</div>
            <div id="recProgressbar" class="progress-bar" style="display: none;"><span id="progressline" style="width: 25%"></span></div>
            <div id="recTimer" class="playback" style="display: none;">2 min.</div>

            <div id="recStore" class="store" onclick="saveVideo()" style="display: none;">stop</div>
            <div id="recViewback" class="back" onclick="playBackVideo()" style="display: none;">back</div>
            <div id="reRecbutton"class="record" onclick="recordButtonClick()" style="display: none;">record</div>
            <div id="videoCountdown" class="vidcountdown3" style="display: none;"></div>
        </article>

        <div class="clear">

    </section>

    <aside id="videoText">
        <!--<p class="time">2 <sup>min.</sup></p>
        <img src="../img/time.png" alt="" class="label time">      -->
        <br/><br/><br/><br/><br/><br/>
        <p style="vertical-align: middle;">Uw persoonlijke videoboodschap mag maximaal <b>2 minuten</b> lang zijn.</p>
    </aside>

    <ul id="nav">
        <li><a title="" class="button" onclick="document.forms['productDetailForm'].submit();">Terug</a>
        <li><a href="../index.jsp" title="" class="button">Opnieuw starten</a>
    </ul>

    <form id="productDetailForm" name="productDetailForm" action="GetProduct" method="post">
        <input type="hidden" name="productCode" value="<%= request.getParameter("p") %>">
    </form>
    <form id="productVideoForm" name="productVideoForm" action="SaveProductVideo" method="post">
        <input type="hidden" name="productCode" value="<%= request.getParameter("p") %>">
        <input type="hidden" name="recordedStream" id="recordedStream" value="">
    </form>
</div>

</body>
</html>