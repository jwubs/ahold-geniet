<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
        <title><s:property value="message" /></title>
    </head>
    <body>
        <h2><s:property value="message" /></h2>
        <ul>
            <li><a href="AdminProductToevoegen">Product toevoegen</a></li>
        </ul>
    </body>
</html>