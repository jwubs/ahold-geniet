<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Geniet! etos</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="../css/normalize.css" type="text/css">
    <link rel="stylesheet" href="../css/stylesheet.css" type="text/css">

    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/jquery.cycle2.min.js"></script>
    <script src="../js/global.js"></script>

    <script type="text/javascript">
        setTimeout(function(){
                location.href='../index.jsp';
            },10000);


        $(document).ready(function(){

            init('homepage');
        });

    </script>
</head>
<body class="scene5 screen2">

<div id="container">

    <header>
        <img src="../img/geniet-etos.png" alt="">
        <h1>het geschenk</h1>
        <h2>Het geschenk</h2>
    </header>

    <section>

        <div class="steps">
            <img src="../img/bar-step3.png" alt="" class="step">
            <ul class="step-detail">
                <li>Uw product</li>
                <li>Persoonlijke boodschap</li>
                <li class="hidden">Het geschenk</li>
            </ul>
        </div>

        <img src="../img/badge-label-small.png" alt="" class="badge small">

        <article>
            <figure><img src="../img/illustration.png" alt=""></figure>
            <h3>Uw geschenk is klaar om te geven</h3>
            <p class="intro">U heeft uw geschenk succesvol gepersonaliseerd.</p>
            <p>Het cadeau en de persoonlijke boodschap zijn gecombineerd in een video die van uw product een bijzonder persoonlijk geschenk maken.<br>Houd er rekening mee dat het 30 minuten kan duren voor de video's beschikbaar zijn</p>
            <div class="clear"></div>
            <p class="end">U kunt nu met het cadeau en de QR code naar de kassa!</p>
        </article>
    </section>

    <a href="../index.jsp" title="" class="button green" id="proceed">Terug naar startscherm</a>

</div>

</body>
</html>
