<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Geniet! etos</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="../css/normalize.css" type="text/css">
    <link rel="stylesheet" href="../css/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="../css/jquery-ui.css" type="text/css">
    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/jquery.cycle2.min.js"></script>
    <script src="../js/jquery-ui-min.js"></script>
    <script src="../js/idle-timer.js"></script>
    <script src="../js/global.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){

            init(60000);

        });

        function timeExpired()
        {
            location.href="../index.jsp";
        }

        var qrScanned = false;
        function qrScan()
        {
            if(!qrScanned)
            {
                qrScanned = true;
                setTimeout(function(){
                    qrScanned = false;
                    document.forms[0].submit();
                },2000);
            }
        }
    </script>
</head>
<body class="scene5 screen1" onload="document.getElementById('qrCode').focus()">

<div id="container">

    <header>
        <img src="../img/geniet-etos.png" alt="">
        <h1>het geschenk</h1>
        <h2>Het geschenk</h2>
    </header>

    <section>

        <div class="steps">
            <img src="../img/bar-step3.png" alt="" class="step">
            <ul class="step-detail step3">
                <li>Uw product</li>
                <li>Het geschenk</li>
                <li class="hidden">Het geschenk</li>
            </ul>
        </div>

        <img src="../img/badge-label.png" alt="" class="badge">

        <article>
            <div style="position: absolute; top: 10px; left: 100px;">
                <img src="../img/scanner.png" alt="">
            </div>
            <div style="position: absolute; top: -15px; left: 850px;">
                <img src="../img/qrhand.png" alt="">
            </div>
            <div style="position: absolute; top: 60px; left: 400px;">
                <p>Scan nu de QR-code<br/>
                op het kaartje en<br/>
                de videoboodschap<br/>
                is gekoppeld<br/>
                </p>
            </div>
            <br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/>
        </article>
        <br/><br/><br/>
    </section>

    <ul id="nav">
        <li><a href="Start" title="" class="button">Terug</a>
        <li><a href="../index.jsp" title="" class="button">Opnieuw starten</a>
    </ul>
    <form action="FinishProductGift" method="post">
        <div style="position: absolute; left: -610px; top: -80px; height: 1px; width: 1px; padding: 1em;">
            <input type="hidden" name="productCode" value="<%= request.getParameter("productCode") %>">
            <input type="hidden" name="recordedStream" value="<s:property value="recordedStream" />">
            <input type="text" id="qrCode" name="qrCode" size="20" onkeypress="qrScan()">
        </div>
        <!--
        <input type="button" value="Scan" onclick="document.getElementById('productCode').value='5410013108009';productScan();">
        -->
    </form>
</div>

<div id="dialog">
    <div id="close-dialog"><span>SLUIT VENSTER</span></div>
    <h3>Wilt u doorgaan?</h3>
    <div id="counter"></div>
    <p>Klik dan ‘Ga verder’. Zo niet dan brengen wij u terug naar het  startscherm binnen 10 seconden.</p>
    <a class="button green" onclick="$('#dialog').dialog('close');document.getElementById('qrCode').focus();">GA VERDER</a>
</div>
</body>
</html>