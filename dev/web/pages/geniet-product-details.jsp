<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Geniet! etos</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="../css/normalize.css" type="text/css">
    <link rel="stylesheet" href="../css/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="../css/jquery-ui.css" type="text/css">
    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/jquery.cycle2.min.js"></script>
    <script src="../js/jquery-ui-min.js"></script>
    <script src="../js/idle-timer.js"></script>
    <script src="../js/global.js"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            init(60000);

        });

        function timeExpired()
        {
            location.href="../index.jsp";
        }

    </script>
</head>
<body class="scene3 screen2">

<div id="container">

    <header>
        <img src="../img/geniet-etos.png" alt="">
        <h1>uw parfum</h1>
        <h2>Uw parfum</h2>
    </header>

    <section>

        <div class="steps">
            <img src="../img/bar-step1.png" alt="" class="step">
            <ul class="step-detail step1">
                <li class="hidden">Uw product</li>
                <li>Persoonlijke boodschap</li>
                <li>Het geschenk</li>
            </ul>
        </div>

        <img src="../img/badge-gift-small.png" alt="" class="badge small">

        <h3><strong><s:property value="product.brand" /></strong>
            <em><s:property value="product.name" /></em>
            <s:property value="product.title" /></h3>

        <article class="image1">
            <figure><img src="../productimages/<s:property value="product.image" />" height="305"></figure>
            <h4>Geuronderdelen</h4>
                <p><s:property value="product.geuronderdelen" /></p>
        </article>

        <article class="image2">
            <figure>
                <iframe width="560" height="305" src="http://www.youtube.com/embed/<s:property value="product.productvideo" />?autoplay=1&rel=0&showinfo=0&controls=0&modestbranding=1&loop=1" frameborder="0"></iframe>
            </figure>
            <h4>Korte beschrijving</h4>
                <p><s:property value="product.description" /></p>
        </article>


        <article class="image3">
            <figure><img src="../productimages/<s:property value="product.spectrum" />"></figure>
            <h4>Geurprofiel</h4>
                <p><s:property value="product.geurprofiel" /></p>
        </article>

        <div class="clear">

    </section>

    <ul id="nav">
        <li><a href="Start" title="" class="button">Terug</a>
        <li><a href="../index.jsp" title="" class="button">Opnieuw starten</a>
    </ul>

    <a href="RecordVideo?p=<s:property value="product.code" />" title="" class="button green" id="proceed">Neem nu je persoonlijke boodschap op</a>

</div>
<div id="dialog">
    <div id="close-dialog"><span>SLUIT VENSTER</span></div>
    <h3>Wilt u doorgaan?</h3>
    <div id="counter"></div>
    <p>Klik dan ‘Ga verder’. Zo niet dan brengen wij u terug naar het  startscherm binnen 10 seconden.</p>
    <a class="button green" onclick="$('#dialog').dialog('close');">GA VERDER</a>
</div>
</body>
</html>