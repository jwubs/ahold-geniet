// de facut preload la close top bar image


function init( _timeout ) {

    var timeout = _timeout; // How many seconds to wait for idle on normal pages
	var counterEnabled = true; 
	var countInitial = 10; // Countdown timer
	var count = countInitial;
	var counter;

	$('#counter').text(countInitial);

	$( "#dialog" ).dialog({ 
		autoOpen: false, 
		height: 522, 
		width: 1067,
		position: { my: "center", at: "center", of: window }, 
		draggable: false,
		resizable: false,
		closeText: "hide"
	});

    $("#dialog").bind('dialogclose', function(event) {
        counterEnabled = false;
        count = countInitial;
        clearInterval(counter);
        $('#counter').text(countInitial);
        init('long');
    });

    $("#dialog").bind('dialogopen', function(event) {
        count = countInitial;
        counterEnabled = true;
        $('#counter').text(countInitial);
    });

	$("#close-dialog, #container").on('click', function(){
		$( "#dialog" ).dialog( "close" );
		$('#counter').text(countInitial);
		clearInterval(counter);
		count = countInitial;
	});
	$(document).bind("idle.idleTimer", function(){

	    console.log("User is idle");
		$( "#dialog" ).dialog( "open" );

		clearInterval(counter);

	    if ( counterEnabled == true ) {
			counter = setInterval(timer, 1000); //1000 will  run it every 1 second
		}

		function timer() {
		  count = count-1;
		  console.log(count);
		  console.log(timeout);
		  if (count <= 0) {
            try {
                timeExpired();
            } catch(e){}

		    clearInterval(counter);

		    counterEnabled = false;

		    return;
		  }

		  $('#counter').text(count);
		}


	});

	$.idleTimer(timeout);

}

