<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Geniet! etos</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="./css/normalize.css" type="text/css">
    <link rel="stylesheet" href="./css/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="./css/jquery-ui.css" type="text/css">
    <script src="./js/jquery-1.8.3.min.js"></script>
    <script src="./js/jquery.cycle2.min.js"></script>
    <script src="./js/jquery-ui-min.js"></script>
    <script src="./js/idle-timer.js"></script>
    <script src="./js/global.js"></script>

    <script type="text/javascript">

        $(document).ready(function(){

            init(15000);

        });

        function timeExpired()
        {
            location.href="index.jsp";
        }
    </script>
</head>
<body class="scene2">

<div id="container">

    <header>
        <img src="./img/geniet-etos.png" alt="">
        <h1>uitleg van de stappen</h1>
        <h2>Maak van uw cadeau<br> een bijzonder en persoonlijk geschenk</h2>
    </header>


    <section>
        <article>
            <img src="./img/badge-gift.png" alt="" class="badge">
            <dl>
                <dt>Uw parfum</dt>
                <dd>Scan de barcode</dd>
                <dd>Bekijk de parfum info</dd>
            </dl>
        </article>

        <article>
            <img src="./img/badge-video.png" alt="" class="badge">
            <dl>
                <dt>Voeg je persoonlijke videoboodschap toe</dt>
                <dd>Neem je persoonlijke boodschap op</dd>
            </dl>
        </article>

        <article>
            <img src="./img/badge-label.png" alt="" class="badge">
            <dl>
                <dt>Het geschenk</dt>
                <dd>Scan de QR-code</dd>
                <dd>Ga naar de kassa voor een speciale verpakking</dd>
                <dd>Geef je geschenk</dd>
            </dl>
        </article>
    </section>

    <ul id="slider-control"></ul>

    <a href="geniet/Start" title="" class="button green" id="proceed">Start met stap 1</a>

</div>
<div id="dialog">
    <div id="close-dialog"><span>SLUIT VENSTER</span></div>
    <h3>Wilt u doorgaan?</h3>
    <div id="counter"></div>
    <p>Klik dan ‘Ga verder’. Zo niet dan brengen wij u terug naar het  startscherm binnen 10 seconden.</p>
    <a class="button green" onclick="$('#dialog').dialog('close');">GA VERDER</a>
</div>
</body>
</html>
