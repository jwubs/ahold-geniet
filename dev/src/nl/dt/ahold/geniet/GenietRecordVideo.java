package nl.dt.ahold.geniet;

import com.opensymphony.xwork2.ActionSupport;

public class GenietRecordVideo extends ActionSupport {

    public static final String SCREEN_TITLE = "Scan uw product";

    public String execute() throws Exception {
        setMessage(SCREEN_TITLE);
        return SUCCESS;
    }

    private String message;

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}