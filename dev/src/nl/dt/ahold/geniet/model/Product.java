package nl.dt.ahold.geniet.model;

public class Product {

    private String code;
    private String brand;
    private String name;
    private String title;
    private String description;
    private String geuronderdelen;
    private String geurprofiel;
    private String image;
    private String productvideo;

    private String spectrum;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductvideo() {
        return productvideo;
    }

    public void setProductvideo(String productvideo) {
        this.productvideo = productvideo;
    }

    public String getSpectrum() {
        return spectrum;
    }

    public void setSpectrum(String spectrum) {
        this.spectrum = spectrum;
    }

    public String getGeuronderdelen() {
        return geuronderdelen;
    }

    public void setGeuronderdelen(String geuronderdelen) {
        this.geuronderdelen = geuronderdelen;
    }

    public String getGeurprofiel() {
        return geurprofiel;
    }

    public void setGeurprofiel(String geurprofiel) {
        this.geurprofiel = geurprofiel;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
