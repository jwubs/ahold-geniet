package nl.dt.ahold.geniet.model;

public class Gift {

    private int id;
    private String code;
    private String qrlink;
    private String youtubelink;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getQrlink() {
        return qrlink;
    }

    public void setQrlink(String qrlink) {
        this.qrlink = qrlink;
    }

    public String getYoutubelink() {
        return youtubelink;
    }

    public void setYoutubelink(String youtubelink) {
        this.youtubelink = youtubelink;
    }
}
