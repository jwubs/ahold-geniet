package nl.dt.ahold.geniet;

import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import nl.dt.ahold.geniet.model.Product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class GenietSaveVideo extends ActionSupport {

    public static final String SCREEN_TITLE = "Scan de QR code";
    private static SessionFactory factory;

    private String recordedStream;
    private String productCode;
    private Product product;


    public String execute() throws Exception {
        String productVideo = "";
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        Session session = factory.openSession();
        product = (Product)session.get(Product.class, productCode);

        if( product != null) {
            productVideo = product.getProductvideo();
        } else {
            product = new Product();
            product.setCode( "onbekende code" );
            product.setName( "onbekend" );
        }

        YoutubeService youtubeService = new YoutubeService();
        setRecordedStream( youtubeService.uploadToYoube( recordedStream, productVideo ) );
        setMessage(SCREEN_TITLE);
        return SUCCESS;
    }

    private String message;

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getRecordedStream() {
        return recordedStream;
    }

    public void setRecordedStream(String recordedStream) {
        this.recordedStream = recordedStream;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}