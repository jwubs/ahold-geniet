package nl.dt.ahold.geniet;

import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import nl.dt.ahold.geniet.model.Product;

public class GenietProduct extends ActionSupport {

    public static final String SCREEN_TITLE = "Product details";
    private static SessionFactory factory;

    private String message;
    private String productCode;
    private Product product;
    private String productName;

    public String execute() throws Exception {
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        Session session = factory.openSession();
        product = (Product)session.get(Product.class, productCode);

        if( product != null) {
            setProduct( product );
            setMessage( product.getName().replaceAll("&lt;","<").replaceAll("&gt;",">"));
        } else {
            product = new Product();
            product.setCode( "onbekende code" );
            product.setName( "onbekend" );
        }
        return SUCCESS;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}