package nl.dt.ahold.geniet;

import com.google.gdata.client.Service;
//import sample.util.SimpleCommandLineParser;
import com.google.gdata.client.youtube.YouTubeService;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.TextConstruct;
import com.google.gdata.data.media.MediaFileSource;
import com.google.gdata.data.media.mediarss.MediaCategory;
import com.google.gdata.data.media.mediarss.MediaDescription;
import com.google.gdata.data.media.mediarss.MediaKeywords;
import com.google.gdata.data.media.mediarss.MediaPlayer;
import com.google.gdata.data.media.mediarss.MediaTitle;
import com.google.gdata.data.youtube.CommentEntry;
import com.google.gdata.data.youtube.PlaylistEntry;
import com.google.gdata.data.youtube.PlaylistFeed;
import com.google.gdata.data.youtube.PlaylistLinkEntry;
import com.google.gdata.data.youtube.PlaylistLinkFeed;
import com.google.gdata.data.youtube.UserEventEntry;
import com.google.gdata.data.youtube.UserEventFeed;
import com.google.gdata.data.youtube.VideoEntry;
import com.google.gdata.data.youtube.VideoFeed;
import com.google.gdata.data.youtube.YouTubeMediaGroup;
import com.google.gdata.data.youtube.YouTubeNamespace;
import com.google.gdata.data.youtube.YtPublicationState;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: johanwubs
 * Date: 23-01-13
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
public class YoutubeService {

    /**
     * Input stream for reading user input.
     */
    private static final BufferedReader bufferedReader = new BufferedReader(
            new InputStreamReader(System.in));

    /**
     * The name of the server hosting the YouTube GDATA feeds.
     */
    public static final String YOUTUBE_GDATA_SERVER = "http://gdata.youtube.com";


    /**
     * The URL of the videos feed
     */
    public static final String VIDEOS_FEED = YOUTUBE_GDATA_SERVER
            + "/feeds/api/videos";

    /**
     * The prefix of the user feeds
     */
    public static final String USER_FEED_PREFIX = YOUTUBE_GDATA_SERVER
            + "/feeds/api/users/";

    /**
     * The prefix of recent activity feeds
     */
    public static final String ACTIVITY_FEED_PREFIX = YOUTUBE_GDATA_SERVER
            + "/feeds/api/events";

    /**
     * The URL suffix of the test user's uploads feed
     */
    public static final String UPLOADS_FEED_SUFFIX = "/uploads";

    /**
     * The URL suffix of the test user's favorites feed
     */
    public static final String FAVORITES_FEED_SUFFIX = "/favorites";

    /**
     * The URL suffix of the test user's playlists feed
     */
    public static final String PLAYLISTS_FEED_SUFFIX = "/playlists";

    /**
     * The URL suffix of the friends activity feed
     */
    public static final String FRIENDS_ACTIVITY_FEED_SUFFIX = "/friendsactivity";

    /**
     * The default username.
     */
    public static final String DEFAULT_USER = "default";

    /**
     * The default video to use for examples.
     */
    public static final String DEFAULT_VIDEO_ID = "scoMN8DYkCw";

    /**
     * The URL used to upload video
     */
    public static final String VIDEO_UPLOAD_FEED =
            "http://uploads.gdata.youtube.com/feeds/api/users/"
                    + DEFAULT_USER + "/uploads";


    public static final String VIDEOLIST_UPLOAD_FEED = "http://gdata.youtube.com/feeds/api/users/default/playlists";
    //http://gdata.youtube.com/feeds/api/users/default/playlists
    /**
     * Enum to deal with various playlist operations:
     *    VIEW = print user's playlists
     *    LIST = print contents of a playlist
     *    CREATE = create new playlist
     *    ADD = add video to playlist
     */
    private enum PlaylistOperation {
        VIEW, LIST, CREATE, ADD
    }


    /**
     * Uploads a new video to YouTube.
     *
     * @param service An authenticated YouTubeService object.
     * @throws IOException Problems reading user input.
     */
    private String uploadVideo(YouTubeService service, String videostream, String productVideo) throws IOException {

        File videoFile = new File("C:\\videostreams\\"+videostream+".flv");
        //File videoFile = new File("/Users/johanwubs/red5/webapps/geniet/streams/" + videostream + ".flv");

        if (!videoFile.exists()) {
            System.out.println("Video " + videostream + " doesn't exist.");
            return "";
        }
        String mimeType = "video/x-flv";
        String videoTitle = videostream;
        String videoId = "";

        VideoEntry newEntry = new VideoEntry();
        PlaylistLinkEntry newPlEntry = new PlaylistLinkEntry();
        newPlEntry.setTitle(new PlainTextConstruct("Geniet! "+videostream));

        YouTubeMediaGroup mg = newEntry.getOrCreateMediaGroup();

        mg.addCategory(new MediaCategory(YouTubeNamespace.CATEGORY_SCHEME, "Tech"));
        mg.setTitle(new MediaTitle());
        mg.getTitle().setPlainTextContent(videoTitle);
        mg.setKeywords(new MediaKeywords());
        mg.getKeywords().addKeyword("gdata-test");
        mg.setDescription(new MediaDescription());
        mg.getDescription().setPlainTextContent(videoTitle);
        MediaFileSource ms = new MediaFileSource(videoFile, mimeType);
        newEntry.setMediaSource(ms);

        VideoEntry createdEntry;
        VideoEntry userVideoEntry;
        VideoEntry productVideoEntry;
        PlaylistEntry playlistEntry;
        String listUrl = "";
        try {
            createdEntry = service.insert(new URL(VIDEO_UPLOAD_FEED), newEntry);
            videoId = createdEntry.getId().split(":")[3];
            PlaylistLinkEntry createdPlEntry = service.insert(new URL(VIDEOLIST_UPLOAD_FEED), newPlEntry);

            userVideoEntry = service.getEntry(new URL("http://gdata.youtube.com/feeds/api/videos/"+videoId), VideoEntry.class);
            playlistEntry = new PlaylistEntry(userVideoEntry);
            service.insert(new URL(createdPlEntry.getFeedUrl()), playlistEntry);

            productVideoEntry = service.getEntry(new URL("http://gdata.youtube.com/feeds/api/videos/"+productVideo), VideoEntry.class);
            playlistEntry = new PlaylistEntry(productVideoEntry);
            service.insert(new URL(createdPlEntry.getFeedUrl()), playlistEntry);

            listUrl = "http://www.youtube.com/embed/"+ videoId + "?list="+ createdPlEntry.getPlaylistId();
            System.out.println("playlist is " + listUrl);
        } catch (ServiceException se) {
            System.out.println("Youtube upload invalid:");
            System.out.println(se.getResponseBody());

            System.out.println("+ " + se.getMessage());
            System.out.println("+ " + se.getDebugInfo());
            return "";
        }



        System.out.println("Video uploaded successfully! Videolink is " + listUrl );
        return listUrl;
    }


    public String uploadToYoube(String video, String productVideo) {
        String username = "geniet.trial@gmail.com";
        String password = "geniettrial";
        //String developerKey = "AI39si6Skd8elwxopD2dgv9YjiMCLLejsXaEpJ0Rx_r2wn8a073Nc5p62Md4V5T5ZkgfyS2nEQbNlkS3vgchDGWYMyVh_dx6CA";
        String developerKey = "AI39si6e7hayEOx4sq32gQgBWo0A9dCsFu_qgouIFbsMxDaLMN6G5BRA-d2d6JNC67Uohi9bMNfU3HzV4MzX52UVlI9BPiwLfw";
        String result = "";

        YouTubeService service = new YouTubeService("geniet",developerKey);
        try {
            service.setUserCredentials(username, password);
        } catch (AuthenticationException e) {
            System.out.println("Invalid login credentials.");
            System.out.println("+ " + e.getMessage());
            System.out.println("+ " + e.getDebugInfo());

        }

        try {
            result = uploadVideo(service, video, productVideo);

        } catch (IOException e) {
                // Communications error
                System.err.println(
                        "There was a problem communicating with the service.");
                e.printStackTrace();
        }

        return result;
    }
}
