package nl.dt.ahold.geniet.video;

import org.red5.server.adapter.ApplicationAdapter;
import org.red5.server.api.IConnection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.PrintStream;
import org.red5.server.adapter.ApplicationAdapter;
import org.red5.server.api.IConnection;
import org.red5.server.api.IScope;
import org.red5.server.api.stream.IServerStream;
import org.red5.server.api.stream.IStreamCapableConnection;
import org.red5.server.api.stream.ISubscriberStream;
import org.red5.server.api.stream.support.SimpleConnectionBWConfig;
import org.slf4j.Logger;


public class Application extends ApplicationAdapter
{
    private static final Log log = LogFactory.getLog( Application.class );
    private IScope appScope;
    private IServerStream serverStream;

    public boolean appStart ( IScope app )
    {
        System.out.println("Geniet appStart");
        appScope = app;
        return true;
    }

    public void appStop ( )
    {
        log.info( "Geniet.recordStop" );
    }

    public boolean appConnect(IConnection conn, Object params[])
    {
        System.out.println("Geniet appConnect");
        measureBandwidth(conn);
        if(conn instanceof IStreamCapableConnection)
        {
            IStreamCapableConnection streamConn = (IStreamCapableConnection)conn;
            SimpleConnectionBWConfig bwConfig = new SimpleConnectionBWConfig();
            bwConfig.getChannelBandwidth()[3] = 0x100000L;
            bwConfig.getChannelInitialBurst()[3] = 0x20000L;
            streamConn.setBandwidthConfigure(bwConfig);
        }
        return super.appConnect(conn, params);
    }

    public void appDisconnect( IConnection conn , Object[] params )
    {
        System.out.println("Geniet appDisconnect");
        if(appScope == conn.getScope() && serverStream != null)
            serverStream.close();
        super.appDisconnect(conn);
    }

    public void streamSubscriberClose(ISubscriberStream stream) {
       System.out.println("Geniet streamSubscriberClose");
    }

    public void FCUnpublish(){
        System.out.println("UNPUBLISH!!!");
    }

    public void saveVideo() {
        System.out.println("Geniet call save video");
    }
}
