package nl.dt.ahold.geniet;

import com.opensymphony.xwork2.ActionSupport;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import nl.dt.ahold.geniet.model.Product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class GenietFinishGift extends ActionSupport {

    private String qrCode;
    private String productCode;
    private Product product;
    private String recordedStream;

    public static final String SCREEN_TITLE = "Gift ready";
    private static SessionFactory factory;

    public String execute() throws Exception {
        System.out.println("Scanned QR code is " + qrCode);
        setMessage(qrCode.split("=")[1]);
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        Session session = factory.openSession();
        product = (Product)session.get(Product.class, productCode);

        if( product != null) {
            System.out.println("connecting to url "+"http://217.198.20.184/insertgift.jsp?p="+qrCode.split("=")[1]+"&l="+recordedStream);
          //  try {

                URL myURL = new URL("http://217.198.20.184/insertgift.jsp?p="+qrCode.split("=")[1]+"&l="+recordedStream);
                URLConnection myURLConnection = myURL.openConnection();
                myURLConnection.connect();
                BufferedReader br = new BufferedReader(
                    new InputStreamReader(myURLConnection.getInputStream()));
          //  }
          //  catch (MalformedURLException e) {
          //      System.out.println(e.toString());
          //  }
          //  catch (IOException e) {
          //      System.out.println(e.toString());
          //  }

        } else {
            product = new Product();
            product.setCode( "onbekende code" );
            product.setName( "onbekend" );
        }
        return SUCCESS;
    }

    private String message;

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }


    public String getRecordedStream() {
        return recordedStream;
    }

    public void setRecordedStream(String recordedStream) {
        this.recordedStream = recordedStream;
    }
}